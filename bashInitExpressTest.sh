#!/bin/bash
rm -rf appTestNodeJs;
mkdir appTestNodeJs;
cd appTestNodeJs;
echo -e "initialize npm and package.json file\n";
npm init -y;
echo -e "\nnpm init done.\nExpress installing\n";
npm install express --save
echo -e "\nExpress Installed\nmaking files ...\n";
touch server.js index.html;
echo "var express = require('express');
var app = express();
var path = require('path');

// viewed at http://localhost:3000
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

app.listen(3000);
console.log('node is running on port 3000, to see -> please open http://localhost:3000/ in your browser');
" > "server.js";
echo "<!DOCTYPE html>
<html lang=en>
<head>
<meta charset="UTF-8">
    <title>testSite</title>
</head>
<body>
<h1>Hello World ...</h1>
</body>
</html>" > "index.html";
node server.js;
echo -e "done\n";
