# nodejs express sample bash-script file for linux
this is sample file that we can use it for start nodejs express in linux. it make directory and two file (index.html and server.js) and package.json and node_modules Directory (to add nodejs Express package). -> 'for beginers in node js'

## Install Dependency
```
 $ npm install

```

## Make Executable Bash-Script File
```
 $ sudo chmod +x bashInitExpressTest.sh

```

## Build & Start Project (simple way)
```
 $ ./bashInitExpressTest.sh

```

## Just Start Watching Node
```
 $ node appTestNodeJs/server.js

```

and you can open your browser on `http://localhost:3000/`

## Authors
* **Ali RoshanZamireGolAfzani**

